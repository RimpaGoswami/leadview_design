/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import 'react-native-gesture-handler';

import { NavigationContainer } from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import MainTab from './src/components/MainTab'
import Signin from './src/screens/Signin'
import QR_Scan from './src/screens/QR_Scan'
import OTP from './src/screens/Otp'
import Catalog from './src/screens/Catalog'
const Stack=createStackNavigator()




 function App() {
  return (
    <NavigationContainer >
       <Stack.Navigator headerMode='none' >
      <Stack.Screen name="Signin" component={Signin}
        
        />
        <Stack.Screen name="QR_Scan" component={QR_Scan} />
        <Stack.Screen name="Otp" component={OTP} />

        <Stack.Screen name="Catalog" component={MainTab}/>
       
                
        
        
    </Stack.Navigator>
      
    
    </NavigationContainer>
  );
}

             
export default App;
