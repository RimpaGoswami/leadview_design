
import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'

   
import Dimentions from '../modules/Dimensions'
   

     class Signin extends Component {
        constructor(props){        
            super(props);        
            this.state={    
                email:'',
                hasFocus:false,
                placeholder:'Email'        
                   
            }   
        }
        handleFocus=()=>
        {
            if(this.state.hasFocus===false){
                this.setState({handleFocus:true,
                    placeholder:' '
                
                })

            }
           
        }

     render(){
         return(
             <View style={styles.container}>
                 <Text style={styles.TextstyleOne}>Welcome </Text>
                 <Text style={styles.TextstyleTwo}> Sign In to Continue   </Text>
                 <TouchableOpacity style={styles.buttonOne} icon={require('../assets/icons/qr-code-scan.png')} >
                        <Image
                            source={require('../assets/icons/qr-code-scan.png')}
                             style={styles.buttonIcon}
                     
                         />
                    
                         <Text style={styles.buttonText} onPress={()=>this.props.navigation.navigate('QR_Scan')} >Scan QR Code to Login</Text>
                 </TouchableOpacity>
                 <Text style={styles.TextStyleThree}>or, Insert Email Address to Continue</Text>

                 <TextInput style={this.state.hasFocus? styles.inputBoxFocus:styles.inputBox}
                        onChangeText={(email) => this.setState({email:email})}
                        value={this.state.email}
                        onFocus={this.handleFocus}
                        placeholder={this.state.placeholder}
                        placeholderTextColor = "#313144"
                        selectionColor="#fff"
                        keyboardType="email-address"
                ></TextInput>
                 <TouchableOpacity style={styles.buttonTwo} icon={require('../assets/icons/qr-code-scan.png')} > 
                         <Text style={styles.buttonTextTwo} onPress={()=>this.props.navigation.navigate('Otp')} >Send OTP</Text>
                 </TouchableOpacity>

             </View>

         )
     }   
    }
    const styles = StyleSheet.create({
        container: {
            flex:1,
            backgroundColor:'#E5E5E5'
            
        },
        TextstyleOne:{
           
            color: '#23334C',
            fontSize:35,
            fontFamily: 'StolzlW00-Medium',
            fontWeight:"bold",
            textAlign:'center',
            marginTop:'45%'


        },
        TextstyleTwo:{
            color: '#313144',
            fontSize:15,
            marginTop:5,
            fontFamily:'StolzlW00-Medium',
            fontWeight:"bold",
            textAlign:'center',
        },
        buttonOne: {

            width: 380,
            height:59,
            marginTop:50,
            flexDirection:'row',
            backgroundColor: '#23334C',
            justifyContent:'center',
            alignContent:'center',
            borderRadius: 10,
            marginVertical: 10,
            paddingVertical: 12,
            marginLeft:15
            
           
        },
        buttonText: {
            fontSize: 35,
            fontWeight: "bold",
            color: '#FBFBFF',
            fontFamily: 'StolzlW00-Medium',
            fontSize:12,
            paddingTop:5,
            paddingVertical:10,
            textAlign: 'left',
            marginLeft:10
            

        },
        buttonIcon:{
        
            height:35,
            width:35,
            paddingTop:5,
            marginRight:20
           
            
        },
        TextStyleThree:{
            marginLeft:10,
            fontSize:14,
            fontWeight:'normal',
            color:'#313144',
            fontFamily:'StolzlW00-Book',
            marginTop:15,
            marginBottom:15

        },
        inputBox: {
            
            width: 375,
            height:59,
            backgroundColor: '#eeeeee', 
            marginLeft:5,
            borderRadius: 5,
            alignSelf:'center',
            borderWidth:2,
            borderColor:'#D1D1D7',
            paddingVertical:12,
            paddingHorizontal: 16,
            fontSize: 16,
            color: '#FFFFFF',
            marginVertical: 10,
            alignItems:"center"
        },
        inputBoxFocus: {
            width: 375,
            height:59,
            backgroundColor: '#eeeeee', 
            marginLeft:5,
            borderRadius: 5,
            alignSelf:'center',
            borderWidth:2,
            borderColor:'#67AEE6',
            paddingVertical:12,
            paddingHorizontal: 16,
            fontSize: 16,
            color: '#FFFFFF',
            marginVertical: 10,
            alignItems:"center"
        },
        buttonTwo:{
            width: 180,
            height:40,
            marginTop:40,
            flexDirection:'row',
            backgroundColor: '#23334C',
            justifyContent:'center',
            alignContent:'center',
            alignSelf:'center',
            borderRadius: 30,
            marginVertical: 10,
            
            marginLeft:15

        },
        buttonTextTwo: {
            fontSize: 25,
            fontWeight: "bold",
            color: '#FBFBFF',
            fontFamily: 'StolzlW00-Medium',
            fontSize:12,
            paddingTop:5,
            paddingTop:5,
            paddingVertical:10,
            alignSelf:'center',
            marginLeft:10
            

        },


    })
    export default Signin


