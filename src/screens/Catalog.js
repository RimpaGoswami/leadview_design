import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,FlatList
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title,Searchbar, Card} from 'react-native-paper'

   
import Dimentions from '../modules/Dimensions'
import  Header  from '../components/Header';
import Search from 'react-native-elements';
   

     class Catalog extends Component {
        constructor(props){        
            super(props);        
            this.state={  
                title:'Catalogue' ,
                search:' ' ,
                product:[
                    {ID:'1',Name:'Customer 1', Designation:'Manager',Active:'5h',Add:0},
                    {ID:'2',Name:'Customer 2',Designation:'Executive',Active:'5m',Add:0},
                    {ID:'3',Name:'Customer 3',Designation:'Executive',Active:'5h',Add:1},
                    {ID:'4',Name:'Customer 4',Designation:'Executive',Active:'5h',Add:1},  
                ],
                text:'Add to Quick List'     
            }   
        }
        renderItem = ({ item }) => (

          
                                         <View style={styles.renderContainer}>   
                                                 <Card style={styles.cardView}>
                                                     <View style={styles.renderViewOne}>
                                                             <View style={styles.profImageView}>
                                                                  <Image
                                                                         source={require('../assets/icons/lendview.png')}
                                                                         style={styles.IconOne}
               
                                                                   />
                                                                  <View style={styles.renderViewTwo}>   
                                                                          <Text style={styles.textStyleTwo}>  {item.Name} </Text>
                                                                              {item.Add ?
                                                                          <View style={styles.renderViewThree}>
                                                                                <Image
                                                                                         source={require('../assets/icons/choice.jpg')}
                                                                                         style={styles.IconTwo}
                    
               
                                                                                  />   
                                                                                 <Text style={styles.textStyleThree}>  {this.state.text} </Text>
                                                                         </View> :<Text>  </Text> }
                                                                </View>   
                                                                 <View style={styles.renderViewFour}>
                                                                        <Image
                                                                                 source={require('../assets/icons/front_arrow.png')}
                                                                                 style={styles.IconFour}
                     
                     
                     
                                                                         />
                                                                  </View>
               
                                                             </View>    
           
                                                         </View>
                                                 </Card>
                                         </View> 
         
          
         )
        render(){
            return(
               
               
                <View style={styles.container}>
                   <Header title={this.state.title}/>
                   <View style={{backgroundColor:'#E5E5E5'}}>
               
                        <Searchbar
                             style={styles.searchView}
                             onChangeText={(text)=>this.setState({search:text})}
                             placeholder='Search Products'
                         >
                        </Searchbar>
                        <View style={styles.buttonView}>
                              <TouchableOpacity
                                   style={styles.buttonOne}
                                  ><Text style={styles.buttonTextOne}> Recomended </Text> 
                             </TouchableOpacity>
                             <TouchableOpacity
                                  style={styles.buttonOne}
                                  ><Text style={styles.buttonTextOne}> Wishlist </Text> 

                            </TouchableOpacity>
                             <TouchableOpacity
                                  style={styles.buttonOne}
                                  ><Text style={styles.buttonTextOne}> Previously Order </Text> 

                            </TouchableOpacity>
                       </View>

                    
                        <FlatList
                            data={this.state.product}
                            renderItem={this.renderItem}
                            keyExtractor={(item)=>item.ID}>
                      </FlatList>
                 </View>  
           </View>
              
            )
        }
    }
    const styles = StyleSheet.create({
    container: {
        backgroundColor:'#FFFFFF',
        justifyContent:'center',
        width:'100%'  
    },
    cardView:{
        justifyContent:'center',
       // marginTop:-15,\
       width:'100%'
    },
    buttonView:{
        flexDirection:'row',
        margin:5

    }, 
    renderViewOne:{
        flexDirection:'column',
        margin:15,
        marginTop:5
    },
    renderViewTwo:{
        flexDirection:'column',
        paddingRight:20,
        marginTop:10

    },
    renderViewThree:{
        flexDirection:'row',
        marginTop:3

    },
    renderViewFour:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end'
    },

    TextstyleOne:{
           
        color: '#313144',
        fontSize:18,
        marginTop:20,
        fontFamily: 'StolzlW00-Medium',
        fontWeight:"bold",
        textAlign:'center',
        alignSelf:'center',
        justifyContent:'center',
        marginRight:5
    },
    textStyleTwo:{
    
        fontSize:16,
        color: '#313144',
        fontWeight: "bold",
        fontFamily:'StolzlW00-Book',
        marginTop:15,
        marginLeft:10
      //  alignSelf:'center',
       // justifyContent:'center',
        //alignContent:'center',
    },
    buttonOne:{
        height:40,
        flexWrap:'wrap',
        backgroundColor: '#23334C',
        justifyContent:'center',
        alignContent:'center',
        textAlign:'center',
        alignSelf:'center',
        borderRadius: 30,
        margin:5
    },
    buttonTextOne: {
        
        fontWeight: "normal",
        color: '#FFFFFF',
        fontFamily: 'StolzlW00-Book',
        fontSize:14,
       justifyContent:'center',
       alignContent:'center',
        alignSelf:'center',
        margin:25
    },

    searchView:{
        borderRadius:30,alignItems:'stretch',marginTop:20,margin:5,height:45
      
    },
    renderContainer:{
        flexDirection:'column',
        margin:5,

    },
    profImageView:{
        flexDirection:'row',
        //marginBottom:15

    },
    listRow:{
        alignSelf:'center',
        width:'100%',
        flexDirection:'row',
        borderBottomWidth:0.6,
        borderBottomColor:'black',
        justifyContent:'center',

    },
    IconOne:{
    
        height:60,
        width:80,
       marginTop:10,
        alignSelf:'center'
    },
    IconTwo:{
    
        height:20,
        width:20,
        marginLeft:20,
        alignSelf:'center'
      
    },
    textStyleThree:{
    
        fontSize:12,
        color: '#171728',
        fontWeight: "normal",
        fontFamily:' StolzlW00-Book',
      //  marginTop:15,
        marginLeft:2,
        alignSelf:'center',
       // justifyContent:'center',
        //alignContent:'center',
       
     
    },
    IconFour:{
    
        height:12,
        width:10,
        justifyContent:'flex-end',
        marginLeft:15,
         alignSelf:'center',
         position:'absolute'


    }
})
    export default Catalog