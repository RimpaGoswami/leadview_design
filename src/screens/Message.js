import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,FlatList
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title} from 'react-native-paper'
     import Header from '../components/Header'
   
import Dimentions from '../modules/Dimensions'
   

     class Message extends Component {
        constructor(props){        
            super(props);        
            this.state={    
              title:'Message',
              Chatlist:[
                {ID:'1',Name:'Ronald Richard', Designation:'Manager',Active:'5h'},
                {ID:'2',Name:'Jenny Wilson',Designation:'Executive',Active:'5m'},
                {ID:'3',Name:'Arlene McCoy',Designation:'Executive',Active:'5h'},
             

            ],
                   
            }   
        }
        renderItem = ({ item }) => (

          
                                          <View style={styles.renderContainer}>   
                                                  <View style={styles.listRow} >
                                                      <View style={styles.profImageView}>
                                                            <Image
                                                                  source={require('../assets/icons/user.png')}
                                                                  style={styles.IconOne}
                    
                                                            />
           
                                                        </View>
                                                       <View style={styles.renderViewOne}>
                                                                 <Text style={styles.textStyleTwo}>  {item.Name} </Text>
                                                                 <Text style={styles.textStyleThree}>{item.Designation} </Text>
                                                      </View>
                                                       <View style={styles.renderViewTwo}>
                                                                 <Text style={styles.textStyleFour}>{item.Active} </Text>
                                                       </View>
                                                </View> 
                                         </View>
         )
        render(){
            return(

                     <View style={styles.container}>
                            <Header title={this.state.title}/>
                            <FlatList
                                 data={this.state.Chatlist}
                                 renderItem={this.renderItem}
                                 keyExtractor={(item)=>item.ID}>            
                          </FlatList>
                          <TouchableOpacity
                                 style={styles.buttonOne}
                                 onPress={()=>this.props.navigation.navigate('Signin')}
                          ><Text style={styles.buttonTextOne}> Back </Text> 

                         </TouchableOpacity>
                     </View>
                 )
                }
         }
    const styles = StyleSheet.create({
    container: {
    
        backgroundColor:'#FFFFFF',
        justifyContent:'center'
        
    },
    TextstyleOne:{
           
        color: '#313144',
        fontSize:18,
        marginTop:20,
        fontFamily: 'StolzlW00-Medium',
        fontWeight:"bold",
        textAlign:'center',
        alignSelf:'center',
        justifyContent:'center',
        marginRight:5
        
       
    },
    buttonOne:{
        width: 300,
        height:50,
        marginTop:300,
        flexDirection:'row',
        backgroundColor: '#23334C',
        justifyContent:'center',
        alignContent:'center',
        textAlign:'center',
        alignSelf:'center',
        borderRadius: 30,
        marginRight:10
  

    },
    buttonTextOne: {
        
        fontWeight: "bold",
        color: '#FFFFFF',
        fontFamily: 'StolzlW00-Medium',
        fontSize:13,
       justifyContent:'center',
       alignContent:'center',
        alignSelf:'center',

        

    },
    listRow:{
        alignSelf:'center',
        width:'100%',
        flexDirection:'row',
        borderBottomWidth:0.6,
        borderBottomColor:'black',
        justifyContent:'center',
        paddingVertical:5
        
     

    },
    IconOne:{
    
        height:50,
        width:50,
        alignSelf:'flex-start',
        marginLeft:15,
       // justifyContent:'flex-end',
        // alignContent:'flex-end',
     
    },
    textStyleTwo:{
    
        fontSize:16,
        color: '#313144',
        fontWeight: "800",
        fontFamily:'StolzlW00-Book',
      //  alignSelf:'center',
       // justifyContent:'center',
        //alignContent:'center',
       
     
    },
    textStyleThree:{
       
        fontSize:13,
        fontFamily:'StolzlW00-Book',
        color: '#646480',
        fontWeight: "normal",
        marginTop:3,
        marginLeft:8
    
        

    },
    renderContainer:{
        flexDirection:'column',margin:5

    },
    renderViewOne:{
        flexDirection:'column', marginLeft:10,paddingRight:20,marginBottom:15
    },
    renderViewTwo:{
        flex:1,flexDirection:'row',marginBottom:15
    },
    profImageView:{
        flexDirection:'row',marginBottom:15

    },

    textStyleFour:{
        flex:1,
        fontSize:12,
        fontFamily:'StolzlW00-Book',
        color: '#646480',
        fontWeight: "normal",
       // alignContent:'flex-end',
       // justifyContent:'flex-end',
       // marginTop:3,
        marginRight:20,
        textAlign:'right'


    
        

    },
})
    export default Message