import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title} from 'react-native-paper'

   
import Dimentions from '../modules/Dimensions'
   

     class Otp extends Component {
        constructor(props){        
            super(props);        
            this.state={    
                email:'',
                hasFocus:false,
                placeholder:'Email' ,
                pin1:'',
                pin2:'',
                pin3:'',
                pin4:'',
                pin5:'',
                pin6:'',
                  
                   
            }   
        }
      componentDidMount=()=>{
          //this.refs.nameref.focus()

      }  
        render(){
            return(
                <View style={styles.container}>
                    <ScrollView>
                  
                    <Image
                     source={require('../assets/icons/enter_otp.png')}
                     style={styles.buttonIcon}
                     size={100}
                 


                    />
                    <Text style={styles.TextstyleOne}>Enter OTP</Text>
                  <Text style={styles.TextstyleTwo}>Enter the 6 digit OTP that was send to   </Text>
                  <Text style={styles.TextstyleThree}>john.doe@gmail.com</Text>

                  <View style={styles.containerTwo}>
                      <TextInput style={styles.OTPInput} 
                      onChangeText={(pin1)=>this.setState({pin1:pin1})}
                      value={this.state.pin1}
                      maxLength={1}
                      
                      >


                      </TextInput>
                      <TextInput style={styles.OTPInput}
                          onChangeText={(pin2)=>this.setState({pin2:pin2})}
                          value={this.state.pin2}
                          maxLength={1}>


                     </TextInput>
                     <TextInput style={styles.OTPInput}
                         onChangeText={(pin3)=>this.setState({pin3:pin3})}
                         value={this.state.pin3}
                         maxLength={1}
                         placeholderTextColor='#313144'
                         >


                    </TextInput>
                    <TextInput style={styles.OTPInput}
                        onChangeText={(pin4)=>this.setState({pin4:pin4})}
                        value={this.state.pin4}
                        maxLength={1}>


                    </TextInput>
                    <TextInput style={styles.OTPInput}
                        onChangeText={(pin5)=>this.setState({pin5:pin5})}
                        value={this.state.pin5}
                        maxLength={1}>


                    </TextInput>
                    <TextInput style={styles.OTPInput}
                        onChangeText={(pin6)=>this.setState({pin6:pin6})}
                        value={this.state.pin6}
                        maxLength={1}>


                    </TextInput>



                  </View>

                  <TouchableOpacity style={styles.buttonTwo} onPress={()=>this.props.navigation.navigate('Catalog')} >
                   
                    
                         <Text style={styles.buttonTextTwo} >Verify Account</Text>
                 </TouchableOpacity>
                 <Text style={styles.Textstylelast}>Have You not received the code? <Text style={{fontWeight:'bold'}}> Resend OTP </Text> </Text>
                 </ScrollView>

                </View>

            
            )
            }
    }

    const styles = StyleSheet.create({
        container: {
            flex:1,
            backgroundColor:'#E5E5E5',
            
            
        },
        containerTwo:{
            flex:0.6,
            justifyContent: "space-evenly",
            flexDirection:'row',
            margin:10,
            marginTop:10,
            padding:5


        },
        TextstyleOne:{
           
            color: '#313144',
            fontSize:18,
            marginTop:20,
            fontFamily: 'StolzlW00-Medium',
            fontWeight:"bold",
            textAlign:'center',
            alignSelf:'center',
            
            justifyContent:'center',
            marginRight:5
            
           
        },
        OTPInput:{
            backgroundColor:'#E6E6E6',
            fontWeight:"600",
            alignSelf:'center',
            textAlign:'center',
            fontSize:18,
            fontWeight:'bold',
            fontFamily: 'StolzlW00-Medium',
            height:45,
            width:40,
            borderRadius:3,
            borderWidth:0.5,
            borderColor:'grey'



        },
        TextStyleOtpPlaceholder:{
            backgroundColor:'#313144',
            fontFamily: 'StolzlW00-Medium',
            fontWeight:"500",
            fontSize:18


        },
        TextstyleTwo:{
            color: '#313144',
            fontSize:13,
            marginTop:20,
            fontFamily:'StolzlW00-Book',
            fontWeight:"normal",
            textAlign:'center',
        },
        TextstyleThree:{
            color: '#313144',
            fontSize:13,
            marginBottom:40,
            marginTop:5,
            fontFamily:'StolzlW00-Book',
            fontWeight:"normal",
            textAlign:'center',
        },
        iconView:{
            height:30,
            width:30
        },
       
        buttonIcon:{
            
            alignSelf:'center',
            justifyContent:'center',
            marginTop:80,
            width:170,
            height:170,
            alignContent:'center',
            borderRadius:170/2,
            paddingTop:5,
            marginRight:20,
            borderWidth:30,
            borderColor:'#CCCFD2',
            marginLeft:10
           
            
        },
        buttonTwo:{
            width: 220,
            height:50,
            marginTop:70,
            flexDirection:'row',
            backgroundColor: '#23334C',
            justifyContent:'center',
            alignContent:'center',
            textAlign:'center',
            alignSelf:'center',
            borderRadius: 30,
            marginRight:10
        
            

        },
        buttonTextTwo: {
            fontSize: 25,
            fontWeight: "bold",
            color: '#FFFFFF',
            fontFamily: 'StolzlW00-Medium',
            fontSize:12,
           justifyContent:'center',
           alignContent:'center',
            alignSelf:'center',
   
            

        },
        Textstylelast:{
            flexDirection:'column',
            color: '#313144',
            fontSize:13,
            marginTop:80,
            fontFamily:'StolzlW00-Book',
            fontWeight:"normal",
            textAlign:'center',
            alignSelf:'center'
        },

    })
    export default Otp