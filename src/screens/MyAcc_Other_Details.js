import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,Linking,FlatList
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title,Card} from 'react-native-paper'
     import QRCodeScanner from 'react-native-qrcode-scanner'

   
import Dimentions from '../modules/Dimensions'
import My_Acc_Header from '../components/My_Acc_Header'

class MyAcc_Other_Details extends Component {
    constructor(props){        
        super(props)        
        this.state={
            Email:'lorem.ipsum@gmail.com',  
            ph_no:'+91 9038416275',
           add_ph_no:'+91 9038416279',
           company_Add:'Plot Netguru No. E2-4 Building, SaltLake City,\n kolkata-700091 \n west Bengal,India',
           desg:'Executive',
           language:'English',
           Currency:'USD($)',
           VAT:'102346572326544587445'

           
        }   
    }
    render(){
        return(
            <View>
                <My_Acc_Header/>
                <ScrollView>
                <Card style={styles.cardViewOne}>
                    <Card style={{}} >
                     <View>
                           
                                    <View style={styles.iconView}>
                                        <Image
                                                             source={require('../assets/icons/address.png')}
                                                             style={styles.IconOne}
                                         />
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={styles.textStyleTwo}> Company Address </Text> 
                                        <Text style={styles.textStyleThree}> {this.state.company_Add} </Text>
       
                                   </View>
                                 
                             
                     </View>        
                     </Card>
                <View style={{marginTop:10}}>
                             <View style={styles.viewOne}>
                                    <View style={styles.iconViewTwo}>
                                        <Image
                                                             source={require('../assets/icons/preferred_language.png')}
                                                             style={styles.IconOne}
                                         />
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={styles.textStyleTwo}> Preferred Language</Text> 
                                        <Text style={styles.textStyleThree}> {this.state.language} </Text>
       
                            </View>
                            </View>
                                    

                     </View>
                     <View style={{marginTop:10}}>
                             <View style={styles.viewTwo}>
                                    <View style={styles.iconViewTwo}>
                                        <Image
                                                             source={require('../assets/icons/currency.png')}
                                                             style={styles.IconOne}
                                         />
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={styles.textStyleTwo}> Preferred Currency</Text> 
                                        <Text style={styles.textStyleThree}> {this.state.Currency} </Text>
       
                            </View>
                            </View>
                                    

                     </View>
                     <View style={{marginTop:10}}>
                             <View style={styles.viewTwo}>
                                    <View style={styles.iconViewTwo}>
                                        <Image
                                                             source={require('../assets/icons/tax_id.png')}
                                                             style={styles.IconOne}
                                         />
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={styles.textStyleTwo}>Tax ID(VAT) </Text> 
                                        <Text style={styles.textStyleThree}> {this.state.VAT} </Text>
       
                            </View>
                            </View>
                                    

                     </View>
                  
               </Card>  
               </ScrollView> 
             </View>
        )
    }   
}
const styles = StyleSheet.create({
    iconView:{
      
        height:50,
        width:50,
        backgroundColor:'#E5E5E5',
        justifyContent:'center',
        marginLeft:20,
        marginTop:30,
        borderRadius:30/2,
        flexWrap:'wrap',
       
      

    },
    cardViewOne:{
        backgroundColor:'#FFFFFF',
        width:'100%',
        borderTopRightRadius:40,
        borderTopLeftRadius:40,
        height:'100%',
        justifyContent:'center'

    },
    cardViewTwo:{
        backgroundColor:'#FFFFFF',
        width:380,
        borderRadius:25, 
        borderWidth:5,
        borderColor:'#FFF',
        marginLeft:40,
        marginRight:40, 
        alignSelf:'center'

    },
    viewOne:{
        flexDirection:'row',
        marginTop:20,
        marginLeft:15

    },
    viewTwo:{
        flexDirection:'row',
        marginLeft:15

    },
    viewThree:{

    },
    textStyleTwo:{
    
        fontSize:14,
        color: '#111123',
        fontWeight: "normal",
        fontFamily:'StolzlW00-Book',
    
        marginLeft:20,
         marginTop:5
     
     
    },
    textStyleThree:{
    
        fontSize:13,
        color: '#646480',
        fontWeight: "normal",
        fontFamily:'StolzlW00-Book',
        
        marginLeft:20,
        marginBottom:25,
       // marginTop:15
     
     
    },
    IconOne:{
    
        height:30,
        width:30,
        alignSelf:'center',
        marginLeft:20

    },
    IconTwo:{
    
        height:25,
        width:25,
        alignSelf:'center',
        marginLeft:15

    },
    iconViewTwo:{
      
        height:50,
        width:50,
        backgroundColor:'#E5E5E5',
        justifyContent:'center',
        marginLeft:20,
       
        borderRadius:30/2,
        flexWrap:'wrap',
       
      

    },
})
export default MyAcc_Other_Details
