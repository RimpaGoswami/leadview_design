import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,Linking,FlatList
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title,Card} from 'react-native-paper'
  
import HeaderStyle from '../components/HeaderStyle';

class Payment_Due extends Component {
    constructor(props){        
        super(props)        
        this.state={
           title:'Payment Due',
           price:'$3000',
           Orderlist:[
            {ID:'1',OrderID:'#OR478455', Date:'1 May,2020',Late:'24 Days Late'}, 
            {ID:'2',OrderID:'#OR478455', Date:'1 May,2020',Late:'24 Days Late'}, 
            {ID:'3',OrderID:'#OR478455', Date:'1 May,2020',Late:'24 Days Late'}, 
            {ID:'4',OrderID:'#OR478455', Date:'1 May,2020',Late:'24 Days Late'}, 

        ],
           
        
    }
}


renderItem = ({ item }) => (

          
    <View>   
  
            <View style={styles.listRow} >
                <View>
                <View style={styles.renderView}>
                        <Text style={styles.textStyleOne}>{item.OrderID}</Text>
                        <View style={styles.renderViewOne}>
                             <Image
                                     source={require('../assets/icons/order_histry.png')}
                                     style={styles.iconOne}
                    
               
                             />  
                             <Text style={styles.textStyleTwo}>{item.Date}</Text> 
                             <View style={styles.renderViewTwo}>
                                     <Text style={styles.textStyleThree}>{this.state.price}</Text> 
                             </View> 
                        </View>
                        <View style={styles.renderViewOne}>
                             <Image
                                     source={require('../assets/icons/clock.png')}
                                     style={styles.iconTwo}
                    
               
                             />  
                             <Text style={styles.textStyleTwo}>{item.Late}</Text> 
                             <View style={{flexDirection:"row"}}>
                                     <TouchableOpacity style={styles.buttonOne}>
                                            <Text style={styles.buttonTextOne}> Unpaid </Text>
                                    </TouchableOpacity>



                             </View>
                            
                               
                             
                        </View>


                </View>
                <TouchableOpacity style={styles.buttonTwo}>
                                            <Text style={styles.buttonTextTwo}> View Order Details </Text>
                                    </TouchableOpacity>
                
                </View>
             </View>
            </View>    
)                
 

    render(){
        return(
            <View>
                    <HeaderStyle title={this.state.title}/>
                    <FlatList
                                 data={this.state.Orderlist}
                                 renderItem={this.renderItem}
                                 keyExtractor={(item)=>item.ID}>
                    </FlatList>
                    
             </View> 
        )
    }
}  
const styles = StyleSheet.create({ 
listRow:{
    alignSelf:'center',
    width:'100%',
    flexDirection:'row',
    marginTop:10,
    borderBottomWidth:0.6,
    borderBottomColor:'black',
    margin:10

},  
renderView:{
    justifyContent:'center',
    borderBottomColor:"black",
    width:'100%',
    borderBottomWidth:0.6

},
renderViewOne:{
    flexDirection:'row',
    marginTop:10
},
renderViewTwo:{
    flexDirection:'row',
    justifyContent:'center'
},
buttonOne:{
    width: 80,
    height:30,
    paddingBottom:10,
    backgroundColor: '#FFEAE9',
  //  justifyContent:'center',
    textAlign:'center',
    alignSelf:'center',
    borderRadius:5, 
    marginLeft:200,
    marginBottom:15


}, 
buttonTextOne: {
    //fontSize: 25,
    fontWeight: "normal",
    color: '#F44336',
    fontFamily: 'StolzlW00-Book',
    fontSize:14,
    textAlign:'center',
    marginTop:5
  
 
},
buttonTwo:{
    width: 210,
    height:40,
    paddingBottom:10,
    flexDirection:'row',
    borderWidth:1,
    borderColor:'black',
    backgroundColor: '#FFFFFF',
     justifyContent:'center',
    textAlign:'center',
    alignSelf:'center',
    borderRadius:35, 
    marginTop:10,
    marginBottom:10


}, 
buttonTextTwo: {
    //fontSize: 25,
    fontWeight: "normal",
    color: '#23334C',
    fontFamily: 'StolzlW00-Book',
    fontSize:14,
    textAlign:'center',
    alignSelf:'center',
    marginTop:5
  
 
},
textStyleOne:{
    fontSize:13,
    color: '#313144',
    fontWeight: "bold",
    justifyContent:'center',
    fontFamily:'StolzlW00-Medium',
    marginTop:10,
    marginLeft:10
   // marginBottom:25
},
textStyleTwo:{
    fontSize:13,
    color: '#646480',
    fontWeight: "normal",
    justifyContent:'center',
    fontFamily:'StolzlW00-book',
    textAlign:'center',
    
    marginLeft:10,
     //marginBottom:15
},
textStyleThree:{
    fontSize:15,
    color: '#313144',
    fontWeight: "bold",
    fontFamily:'StolzlW00-Medium',
    alignSelf:"center",
    //marginRight:20,
    marginLeft:210,
   // marginBottom:20
  
},
iconTwo:{
    
    height:15,
    width:15,
    marginLeft:15,
    //alignSelf:'center'
  
},
iconOne:{
    
    height:14,
    width:14,
    marginLeft:15,
    //alignSelf:'center'
  
},
})
export default Payment_Due