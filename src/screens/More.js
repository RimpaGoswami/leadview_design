import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,FlatList
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title} from 'react-native-paper'
     import Header from '../components/Header'
    import Dimentions from '../modules/Dimensions'
   

     class More extends Component {
        constructor(props){        
            super(props);        
            this.state={ 
                title:'More',
                More:[
                    {ID:'1',Name:'My Account',image:require('../assets/icons/my_account.png')},
                    {ID:'2',Name:'Preference',image:require('../assets/icons/preference.png')},
                    {ID:'3',Name:'Information',image:require('../assets/icons/information.png')},
                    {ID:'4',Name:'Access',image:require('../assets/icons/access.png')},
                    {ID:'5',Name:'Scan',image:require('../assets/icons/scan.png')},
                    {ID:'6',Name:'Payments Due',image:require('../assets/icons/payments.png')}, 
                    {ID:'7',Name:'Order History',image:require('../assets/icons/order_histry.png')},
                    
                    


                ]  ,         
            }   
        }


        renderItem = ({ item }) => (

          
                                  <View style={styles.renderItemContainer}>  
                                          <View style={styles.listRow} >
                                                 <View style={styles.renderViewOne}>
                                                         <View style={styles.renderViewTwo}>
                                                               <View style={styles.iconView}>
                                                                     <Image
                                                                              source={item.image}
                                                                              style={styles.IconOne}
                                                                     />
                                               
                                                                </View>     
                                                                <Text style={styles.textStyleTwo}>  {item.Name} </Text> 
                                                         </View>
                                                </View> 
                                                 <View style={styles.renderViewThree}>
                                                         <Image
                                                                 source={require('../assets/icons/front_arrow.png')}
                                                                 style={styles.IconTwo}                
                                                          />
                                                 </View>          
                                         </View>
                                    </View>
            ) 
        render(){
            return(
                     <View style={styles.container}>
                             <Header title={this.state.title}/>
                             <View style={styles.flatListView}>
                                  <FlatList
                                       data={this.state.More}
                                       renderItem={this.renderItem}
                                       keyExtractor={(item)=>item.ID}>
                                 </FlatList>
                                 <TouchableOpacity style={styles.buttonOne}>
                                        <Image
                                                 source={require('../assets/icons/logout.png')}
                                                 style={styles.IconThree}

                     
                                         />
                                        <Text style={styles.buttonTextOne}> Logout </Text> 

                                  </TouchableOpacity>
                    
                             </View>
                    </View>
            )
        }
    }
    const styles = StyleSheet.create({
    container: {
      
        backgroundColor:'#FFFFFF',
        justifyContent:'center'
        
    },
    flatListView:{
        backgroundColor:'#E5E5E5',

    },
    renderItemContainer:{
        flexDirection:'column',backgroundColor:'#FFFFFF'
    },
    TextstyleOne:{
           
        color: '#313144',
        fontSize:18,
        marginTop:20,
        fontFamily: 'StolzlW00-Medium',
        fontWeight:"bold",
        textAlign:'center',
        alignSelf:'center',
        justifyContent:'center',
        marginRight:5
              
    },
    textStyleTwo:{
    
        fontSize:14,
        color: '#171728',
        fontWeight: "800",
        fontFamily:'StolzlW00-Book',
        textAlign:'center',
        marginLeft:15,
        marginTop:10
      //  alignSelf:'center',
       // justifyContent:'center',
        //alignContent:'center',
       
     
    },
    listRow:{
     
        width:'100%',
        flexDirection:'row',
        borderBottomWidth:0.6,
        borderBottomColor:'black',
        paddingVertical:5
    },
        
    IconOne:{
    
        height:25,
        width:25,
        alignSelf:'center',
        marginLeft:15

    },
    renderViewOne:{
        flexDirection:'column',marginBottom:10,marginTop:10,marginLeft:5
    },
    renderViewTwo:{
        flexDirection:'row',marginLeft:10,
    },
    renderViewThree:{
        flexDirection:'row',flex:1,justifyContent:'flex-end'

    },
    IconTwo:{
    
        height:12,
        width:10,
        margin:15,
        alignSelf:'center',
    },
    iconView:{
      
        height:40,
        width:40,
        backgroundColor:'#E5E5E5',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:40/2,
        flexWrap:'wrap',
        alignSelf:'center',
      

    },
    buttonOne:{
       
        width:300,
        height:50,
        backgroundColor: '#23334C',
       // justifyContent:'center',
       // alignContent:'center',
        textAlign:'center',
        alignSelf:'center',
        borderRadius: 30,
        marginTop:80,
        flexDirection:'row',
        justifyContent:'center'

    },
    buttonTextOne: {
        
        fontWeight: "normal",
        color: '#FFFFFF',
        fontFamily: 'StolzlW00-Medium',
        fontSize:14,
        textAlign:'center',
        alignSelf:'center',
        margin:10,
     
    
      
    },
    IconThree:{
    
        height:20,
        width:20,
        marginLeft:15,
        alignSelf:'center',
        justifyContent:'center',
        margin:10
    },

})
    export default More