import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,Linking,FlatList
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title,Card} from 'react-native-paper'
     import QRCodeScanner from 'react-native-qrcode-scanner'

   
import Dimentions from '../modules/Dimensions'
import My_Acc_Header from '../components/My_Acc_Header'

class MyAcc_Social_Details extends Component {
    constructor(props){        
        super(props)        
        this.state={
            Facebook:'https://www.facebook.com/leslie.',  
            Insta:'https://www.instagram.com/leslie',
         

           
        }   
    }
    render(){
        return(
            <View>
                <My_Acc_Header/>
                <ScrollView>
                 <Card style={styles.cardView}>
                
                    
                        <View>
                            <View style={styles.viewOne}>
                                    <View style>
                                        <Image
                                                             source={require('../assets/icons/facebook.png')}
                                                             style={styles.IconOne}
                                         />
                                    </View>
                                    <View style={styles.viewTwo}>
                                        <Text style={styles.textStyleTwo}> Facebook </Text> 
                                        <Text style={styles.textStyleThree}> {this.state.Facebook} </Text>
       
                            </View>
                            </View>
                            <View style={styles.viewThree}>
                                    <View style>
                                        <Image
                                                             source={require('../assets/icons/instagram.png')}
                                                             style={styles.IconOne}
                                         />
                                    </View>
                                    <View style={styles.viewFour}>
                                        <Text style={styles.textStyleTwo}> Instagram </Text> 
                                        <Text style={styles.textStyleThree}> {this.state.Insta} </Text>
       
                                     </View>
                        </View>
                    </View>
            </Card>

            </ScrollView>
                
             </View>
        )
    }   
}
const styles = StyleSheet.create({
    iconView:{
      
        height:50,
        width:50,
        backgroundColor:'#E5E5E5',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:30/2,
        flexWrap:'wrap',
        alignSelf:'center',
      

    },
    cardView:{
        backgroundColor:'#FFFFFF',
        width:'100%',
        borderTopRightRadius:40,
        borderTopLeftRadius:40,
        height:'100%'

    },
    viewOne:{
        flexDirection:'row',
        marginTop:20,
        marginLeft:15,
        marginTop:30

    },
    viewTwo:{
        flexDirection:'column'
    },
    viewThree:{
        flexDirection:'row',
        marginTop:30,
        marginLeft:15

    },
    viewFour:{
        flexDirection:'column'
    },

    textStyleTwo:{
    
        fontSize:14,
        color: '#171728',
        fontWeight: "normal",
        fontFamily:'StolzlW00-Book',
    
        marginLeft:10,
     
     
     
    },
    textStyleThree:{
    
        fontSize:14,
        color: '#646480',
        fontWeight: "normal",
        fontFamily:'StolzlW00-Book',
        
        marginLeft:10,
       // marginTop:15
     
     
    },
    IconOne:{
    
        height:25,
        width:25,
        alignSelf:'center',
        marginLeft:20

    },
    IconTwo:{
    
        height:30,
        width:35,
        alignSelf:'center',
        marginLeft:20

    },
})
export default MyAcc_Social_Details
