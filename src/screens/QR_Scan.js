import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,Linking
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title,Card} from 'react-native-paper'
     import QRCodeScanner from 'react-native-qrcode-scanner'

   
import Dimentions from '../modules/Dimensions'
   

     class QR_Scan extends Component {
        constructor(props){        
            super(props)        
            this.state={    
               
                  
                   
            }   
        }

        ifScaneed=e=>{
            Linking.openURL(e.data).catch(err=>alert("Invalid QR code",e.data))
        }

        render()
        {
            return(
                <View>
                    <View style={styles.headerContainer}>
                            <TouchableOpacity onPress={()=>this.props.navigation.navigate('Signin')}>
                                <Image
                                    source={require('../assets/icons/back.jpg')}
                                    style={styles.IconTwo}>
                      
                                </Image>
                            </TouchableOpacity>     
                                 <Text style={styles.headerText}>QR Scanner</Text>
                    </View>
                  
                    <Card style={styles.cardView}>
                        <View style={{flexDirection:'row',justifyContent:'space-evenly'}}> 
                            <Image
                                 source={require('../assets/icons/flash_off.png')}
                                 style={styles.IconOne}
                    
                     
                              />

                             <Image
                                 source={require('../assets/icons/image.png')}
                                 style={styles.IconTwo}
                     
                     
                     
                            />
                        
                      </View>  

                    </Card>
                    <View style={{marginTop:140}}>
                        <QRCodeScanner
                                containerStyle={{backgroundColor:'#FFF'}}
                                onRead={this.ifScaneed}
                                cameraStyle={styles.cameraStyle}
                                reactivate={true}
                                permissionDialogMessage='Need Permission to Access Camera'
                                reactivateTimeout={10}
                                showMarker={true}
                                markerStyle={{borderColor:'#FFF',borderRadius:15,width:100,height:80}}
                        ></QRCodeScanner>


                    </View>


                    <TouchableOpacity style={styles.buttonOne}  >
                   
                    
                   <Text style={styles.buttonTextOne} >Cancel Scanning</Text>
                     </TouchableOpacity>
                    


                </View>
            )
        }
    }





   const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'#E5E5E5',
        
        
        
    },
    cameraStyle:{
        justifyContent:"center",
        width:200,
        height:80,
        borderColor:'grey',
        borderWidth:2,
        alignSelf:'center',
        marginRight:10


    },
    headerContainer:{

        marginTop:20,
        height:40,
        width:'100%',
        marginBottom:15,
        borderBottomColor:'black',
        borderBottomWidth:1,
        flexDirection:'row'

    },
    cardView:{
       
        width: 220,
        height:50,
        marginTop:30,
        flexDirection:'row',
        backgroundColor: '#EBEBEB',
        alignSelf:'center',
        borderRadius: 10,
        marginRight:10
    },
    IconOne:{
        
        height:25,
        width:25,
        marginTop:13,  
        marginRight:20
    },
    IconTwo:{
        
        height:25,
        width:25,
        marginTop:13,
        marginLeft:15,
        justifyContent:'center',
        alignContent:'center'
      
       
        
    },
    buttonOne:{
        width: 300,
        height:50,
        marginTop:300,
        flexDirection:'row',
        backgroundColor: '#23334C',
        justifyContent:'center',
        alignContent:'center',
        textAlign:'center',
        alignSelf:'center',
        borderRadius: 30,
        marginRight:10
  

    },
    buttonTextOne: {
        
        fontWeight: "bold",
        color: '#FFFFFF',
        fontFamily: 'StolzlW00-Medium',
        fontSize:13,
       justifyContent:'center',
       alignContent:'center',
        alignSelf:'center',

        

    },
    headerText:{
        fontWeight: "bold",
        color: '#313144',
        fontFamily: 'StolzlW00-Medium',
        fontSize:16,
       justifyContent:'center',
       margin:15,
       marginBottom:5
      
    },
    IconArrow:{
        
        height:50,
        width:30,
        marginLeft:15,
        marginBottom:20,
        justifyContent:'center',
        margin:20

        
    },

})

    export default QR_Scan