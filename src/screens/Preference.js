import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,Linking,FlatList
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title,Card} from 'react-native-paper'
     import QRCodeScanner from 'react-native-qrcode-scanner'
     import { CheckBox } from 'react-native-elements'

   
import Dimentions from '../modules/Dimensions'
import HeaderStyle from '../components/HeaderStyle';

class Preference extends Component {
    constructor(props){        
        super(props)        
        this.state={
           title:'Preference',
           checkedOne:false,
           checkedTwo:false,
           checkedThree:false,
           buttonChecked:false,
           buttonCheckedTwo:false
        }   
    }
    handleChange=()=>{
        console.log('fuction called')
        if(this.state.checkedOne===false)
                this.setState({checkedOne:true})
        else
                this.setState({checkedOne:false})

    }

    render(){
        return(
            <View>
            <HeaderStyle title={this.state.title}/>
            <ScrollView>
            <View style={styles.viewOne}>
                <View style={{backgroundColor:'#FFFFFF'}}>
                   <Text style={styles.textStyleOne}> Marketing Message Title  </Text>
                   <Card style={styles.cardViewOne} > 
                        <Text style={styles.textStyleTwo}>Marketing Message Goes Here</Text>
                        <View style={styles.checkView}>
                        <CheckBox
                            
                            checkedIcon={<Image style={styles.iconView} source={require('../assets/icons/check_2.png')} />}
                            uncheckedIcon={<Image style={styles.iconView} source={require('../assets/icons/check_box.png')} />}
                            checked={this.state.checkedOne}
                            onChange={()=>{this.setState({checkedOne:!this.state.checkedOne})}}
                           
                        />
                    
                        </View>



                   </Card>
                   <Card style={styles.cardViewOne} > 
                        <Text style={styles.textStyleTwo}>5th Aug</Text>
                        <View style={styles.checkView}>
                        <CheckBox
                            
                            checkedIcon={<Image style={styles.iconView} source={require('../assets/icons/check_2.png')} />}
                            uncheckedIcon={<Image style={styles.iconView} source={require('../assets/icons/check_box.png')} />}
                            checked={this.state.checkedTwo}
                            //onIconPress={() => this.handleChange}
                            onIconPress={()=>{this.setState({checkedTwo:!checkedTwo})}}
                        />
                        </View>



                   </Card>
                   <Card style={styles.cardViewThree} > 
                        <Text style={styles.textStyleTwo}>Email Newsletter</Text>
                        <View style={styles.checkView}>
                        <CheckBox
                            
                            checkedIcon={<Image style={styles.iconView} source={require('../assets/icons/check_2.png')} />}
                            uncheckedIcon={<Image style={styles.iconView} source={require('../assets/icons/check_box.png')} />}
                            checked={this.state.checkedThree}
                            right={true}
                           // onValueChange={() => this.handleChange}
                           onPress={()=>{this.setState({checkedThree:!this.state.checkedThree})}}
                        />
                        </View>



                   </Card>




                </View>
              
                <View style={styles.buttonView}>
                            <Text style={styles.textStyleOne}> Interest  </Text>
                            <View style={styles.buttonViewTwo}> 

                                    <TouchableOpacity style={this.state.buttonChecked? styles.buttonOne:styles.buttonTwo } onPress={()=>this.setState({buttonChecked:!this.state.buttonChecked})} >
                                         <Text style={this.state.buttonChecked?styles.buttonTextThree:styles.buttonTextTwo} >Node</Text>
                                         {this.state.buttonChecked?<Image style={styles.iconViewTick} source={require('../assets/icons/check_white.png')} ></Image>: <Text>  </Text>  }
                                   </TouchableOpacity>
                                   <TouchableOpacity style={this.state.buttonCheckedTwo? styles.buttonOne:styles.buttonTwo } onPress={()=>this.setState({buttonCheckedTwo:!this.state.buttonCheckedTwo})} >
                                         <Text style={this.state.buttonCheckedTwo?styles.buttonTextThree:styles.buttonTextTwo} >AngularJS</Text>
                                         {this.state.buttonCheckedTwo?<Image style={styles.iconViewTick} source={require('../assets/icons/check_white.png')} ></Image>: <Text>  </Text>  }
                                   </TouchableOpacity>

                            </View>
                           

                </View>

              
                

             </View>
            
            <View style={styles.submitView}>
                <TouchableOpacity style={styles.buttonThree}>
                    <Text style={styles.buttonTextThree}> Submit Details </Text>
                </TouchableOpacity>


                </View>

            
                </ScrollView>
            </View>
   
        )
    }

}
const styles = StyleSheet.create({
    viewOne:{
        backgroundColor:'#E5E5E5',
      
        
    },
    checkView:{
        flexDirection:'row',
        flex:1,
        justifyContent:'flex-end'
    },
    buttonView:{
        backgroundColor:'#FFFFFF',
        marginTop:15,
        flex:0.2,
        marginLeft:5
    },
    buttonViewTwo:{
        flexDirection:'row',
        flex:1
    },

    
    submitView:{
        marginTop:100,
        height:80,
        flexDirection:'column',
        justifyContent:'flex-end'

    },
    iconView:{
        height:25,
        width:25,
        justifyContent:'flex-end',
        alignSelf:'flex-end',
        marginBottom:15,
        marginTop:5
    },
    iconViewTick:{
        height:15,
        width:15,
        justifyContent:'flex-end',
        alignSelf:'flex-end',
        marginLeft:80,
        marginBottom:15,
        marginTop:5
    },
    textStyleOne:{
            fontSize:15,
            color: '#313144',
            fontWeight: "bold",
            fontFamily:'StolzlW00-Medium',
            marginTop:20,
            marginLeft:10
           // marginBottom:25
    },
    textStyleTwo:{
        fontSize:13,
        color: '#313144',
        fontWeight: "normal",
        fontFamily:'StolzlW00-book',
        marginTop:12,
        marginLeft:15,
    
        justifyContent:'center',
        alignItems:"center",
        marginBottom:3
},
buttonTwo:{
    width: 180,
    height:45,
    marginTop:20,
    flex:0.5,
    margin:15,
    flexDirection:'row',
    backgroundColor: '#E5E5E5',
  //  justifyContent:'center',
    alignContent:'center',
   alignSelf:'center',
    marginLeft:10,
    borderRadius:5,
},
buttonOne:{
    width: 180,
    height:45,
    marginTop:20,
    flex:0.5,
    margin:15,
    flexDirection:'row',
    backgroundColor: '#23334C',
  //  justifyContent:'center',
    alignContent:'center',
    textAlign:'center',
   alignSelf:'center',
    marginLeft:10,
    borderRadius:5,


    

},
buttonThree:{
    width: 240,
    height:45,
    //marginTop:80,
   // flex:0.5,
    margin:15,
    textAlign:'center',
    //flexDirection:'row',
    backgroundColor: '#23334C',
  //  justifyContent:'center',
    alignContent:'center',
    alignSelf:'center',
    marginLeft:10,
    marginTop:40,
    justifyContent:'center',
    borderRadius:25,
},

buttonTextThree: {
    //fontSize: 25,
    fontWeight: "normal",
    color: '#FFFFFF',
    fontFamily: 'StolzlW00-Medium',
    fontSize:14,
    justifyContent:'center',
    alignSelf:'center',
    textAlign:'left',
    marginLeft:10

},
buttonTextTwo: {
    //fontSize: 25,
    fontWeight: "normal",
    color: '#FFFFFF',
    fontFamily: 'StolzlW00-book',
    fontSize:14,
    justifyContent:'center',
    alignSelf:'center',
    textAlign:'left',
    marginLeft:10
},
buttonTextTwo: {
    //fontSize: 25,
    fontWeight: "normal",
    color: '#313144',
    fontFamily: 'StolzlW00-book',
    fontSize:14,
    justifyContent:'center',
    alignSelf:'center',
    textAlign:'left',
    marginLeft:10
},

    cardViewOne:{
        backgroundColor:'#FFFFFF',
        width:380,
        height:50,
        flexDirection:'row',
       // borderBottomRightRadius:0,
      //  borderBottomLeftRadius:0,
        borderRadius:10, 
        borderWidth:3,
        borderColor:'#FFFFFF',
        marginTop:15, 
        alignSelf:'center',
        justifyContent:"center"

    },
    cardViewThree:{
        backgroundColor:'#FFFFFF',
        width:380,
        height:50,
        flexDirection:'row',
       // borderBottomRightRadius:0,
      //  borderBottomLeftRadius:0,
        borderRadius:10, 
        borderWidth:3,
        borderColor:'#FFFFFF',
        marginTop:15, 
        alignSelf:'center',
        justifyContent:"center",
        marginBottom:20

    },


})
export default Preference