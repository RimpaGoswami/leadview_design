import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,Linking,FlatList
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title,Card} from 'react-native-paper'
     import QRCodeScanner from 'react-native-qrcode-scanner'

   
import Dimentions from '../modules/Dimensions'
import My_Acc_Header from '../components/My_Acc_Header'

class MyAcc_Personal_Details extends Component {
    constructor(props){        
        super(props)        
        this.state={
            Email:'lorem.ipsum@gmail.com',  
            ph_no:'+91 9038416275',
           add_ph_no:'+91 9038416279',
           company:'T-web Exponent Services Private Limited',
           desg:'Executive'

           
        }   
    }


    render(){
        return(
            <View>
                <My_Acc_Header/>
                <ScrollView>
                 <Card style={styles.cardOne}>
                
                    
                        <View>
                            <View style={styles.viewOne}>
                                    <View style={styles.iconView}>
                                        <Image
                                                             source={require('../assets/icons/mail.png')}
                                                             style={styles.IconOne}
                                         />
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={styles.textStyleTwo}> Email </Text> 
                                        <Text style={styles.textStyleThree}> {this.state.Email} </Text>
       
                            </View>
                            </View>
                            <View style={styles.viewOne}>
                                    <View style={styles.iconView}>
                                        <Image
                                                             source={require('../assets/icons/mobile.png')}
                                                             style={styles.IconOne}
                                         />
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={styles.textStyleTwo}> Phone Number </Text> 
                                        <Text style={styles.textStyleThree}> {this.state.ph_no} </Text>
       
                                     </View>
                          
                        
                        </View>

                        <View style={styles.viewOne}>
                                    <View style={styles.iconView}>
                                        <Image
                                                             source={require('../assets/icons/mobile.png')}
                                                             style={styles.IconOne}
                                         />
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={styles.textStyleTwo}> Additional Number </Text> 
                                        <Text style={styles.textStyleThree}> {this.state.add_ph_no} </Text>
       
                                     </View>
                          
                        
                        </View>
                        <View style={styles.viewOne}>
                                    <View style={styles.iconView}>
                                        <Image
                                                             source={require('../assets/icons/company_name.png')}
                                                             style={styles.IconOne}
                                         />
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={styles.textStyleTwo}> Company Name </Text> 
                                        <Text style={styles.textStyleThree}> {this.state.company} </Text>
       
                                     </View>
                          
                        
                        </View>
                        <View style={styles.viewOne}>
                                    <View style={styles.iconView}>
                                        <Image
                                                             source={require('../assets/icons/designation.png')}
                                                             style={styles.IconOne}
                                         />
                                    </View>
                                    <View style={{flexDirection:'column'}}>
                                        <Text style={styles.textStyleTwo}> Designation </Text> 
                                        <Text style={styles.textStyleThree}> {this.state.desg} </Text>
       
                                     </View>
                        </View>
                    </View>
            </Card>
        </ScrollView>
        </View>
        )
    }
}
const styles = StyleSheet.create({
    iconView:{
      
        height:50,
        width:50,
        backgroundColor:'#E5E5E5',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:30/2,
        flexWrap:'wrap',
        alignSelf:'center',
      

    },
    viewOne:{
        flexDirection:'row',
        marginTop:30,
        marginLeft:15
    },
    cardOne:{
        backgroundColor:'#FFFFFF',
        width:'100%',
        borderTopRightRadius:40,
        borderTopLeftRadius:40,
        height:'100%'
    },

    textStyleTwo:{
    
        fontSize:14,
        color: '#171728',
        fontWeight: "normal",
        fontFamily:'StolzlW00-Book',
    
        marginLeft:20,
       // marginTop:15
     
     
    },
    textStyleThree:{
    
        fontSize:14,
        color: '#646480',
        fontWeight: "normal",
        fontFamily:'StolzlW00-Book',
        
        marginLeft:20,
       // marginTop:15
     
     
    },
    IconOne:{
    
        height:25,
        width:25,
        alignSelf:'center',
        marginLeft:20

    },
    IconTwo:{
    
        height:30,
        width:35,
        alignSelf:'center',
        marginLeft:20

    },
})
export default MyAcc_Personal_Details