import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,FlatList
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title,Card} from 'react-native-paper'

   
import Dimentions from '../modules/Dimensions'
import  Header  from '../components/Header';

     class Basket extends Component {
        constructor(props){        
            super(props);        
            this.state={ 
                title:'Basket',   
                Basket:[
                    {ID:'1',Name:'Basket Name 1'},
                    {ID:'2',Name:'Basket Name 2'},
                    {ID:'3',Name:'Basket Name 3'},
                    {ID:'4',Name:'Basket Name 4'}
    
                ],
                basketRef:'cart 3',
                products:8,
                rupees:48.50
              
                   
            }   
        }



        renderItem = ({ item }) => (

          
             <View style={styles.renderContainer}>   
           
                     <View style={styles.listRow} >
                            <View style={styles.renderViewOne}>
                                <View style={styles.renderViewTwo}>
                                        <Text style={styles.textStyleTwo}>  {item.Name} </Text>
                                        <Text style={styles.textStyleThree}> Products: {this.state.products} </Text>
                                </View>
                                 <View style={styles.renderViewThree}>
                                        <Text style={styles.textStyleFour}> Basket Ref: {this.state.basketRef} </Text>  
                                        <Text style={styles.textStyleFive}> $ {this.state.rupees} </Text>
                                </View>
                         </View>
                         <View style={styles.renderViewFour}>
                             <Image
                                 source={require('../assets/icons/front_arrow.png')}
                                 style={styles.IconThree}
                             />
                        </View>
                     </View>
           </View>
           
        )
        render(){
            return(
              <View style={{backgroundColor:'#FFFFFF'}}>
                    <Header title={this.state.title}/>
                   < View style={styles.container}>
                   
                            <FlatList
                                 data={this.state.Basket}
                                 renderItem={this.renderItem}
                                 keyExtractor={(item)=>item.ID}>
                            </FlatList>
                      </View>
                </View>
            )
        }
    }
    const styles = StyleSheet.create({
    container: {

        justifyContent:'center', 
    },
    renderContainer:{
        flexDirection:'column',margin:5
    },
    renderViewOne:{
        flexDirection:'column',marginBottom:20
    },
    renderViewTwo:{
        flexDirection:'row'
    },
    renderViewThree:{
        flexDirection:'row',margin:5

    },
    renderViewFour:{
        flexDirection:'row',flex:1,justifyContent:'flex-end'
    },

    TextstyleOne:{
           
        color: '#313144',
        fontSize:18,
        marginTop:20,
        fontFamily: 'StolzlW00-Medium',
        fontWeight:"bold",
        textAlign:'center',
        alignSelf:'center',
        justifyContent:'center',
        marginRight:5     
    },
    IconOne:{
        
        height:25,
        width:25,
        marginLeft:15,
        justifyContent:'center',
        alignContent:'center'
        
    },
    headerText:{
        fontWeight: "bold",
        color: '#313144',
        fontFamily: 'StolzlW00-Medium',
        fontSize:16,
       justifyContent:'center',
       marginLeft:20 
    },
    headerContainer:{

        marginTop:20,
        marginTop:20,
        height:40,
        width:'100%',
        marginBottom:15,
        borderBottomColor:'black',
        borderBottomWidth:1,
        flexDirection:'row'

    },
    IconTwo:{
    
        height:25,
        width:25,
        alignSelf:'baseline',
        marginLeft:15,
        justifyContent:'flex-end',
         alignContent:'flex-end',
        marginLeft:250   
    },
    cardview:{
        margin:5,
        borderRadius:10,
        backgroundColor:'pink'
    },
    textStyleTwo:{
    
        fontSize:16,
        color: '#313144',
        fontWeight: "bold",
        fontFamily:'StolzlW00-Medium',
   
    },
    textStyleThree:{
       
        fontSize:13,
        fontFamily:'StolzlW00-Book',
        color: '#23334C',
        fontWeight: "normal",
        marginTop:3,
        marginLeft:170,
        

    },
    textStyleFour:{
        fontSize:13,
        fontFamily:'StolzlW00-Book',
        color: '#646480',
        fontWeight: "normal",

    },
    textStyleFive:{
        fontSize:13,
        fontFamily:'StolzlW00-Book',
        color: '#646480',
        fontWeight: "normal",
        marginLeft:175,

    },
    listRow:{
        alignSelf:'center',
        width:'100%',
        flexDirection:'row',
        borderBottomWidth:0.6,
        borderBottomColor:'black',
        margin:10

    },
    IconThree:{
        height:12,
        width:15,
        //alignSelf:'baseline',
       // marginLeft:15,
        justifyContent:'flex-end',
         alignContent:'flex-end',
         alignSelf:'center',
        marginLeft:15  ,
        marginBottom:10 ,
        marginRight:15
    },
    
})
    export default Basket