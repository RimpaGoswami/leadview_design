import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,Linking,FlatList
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title,Card} from 'react-native-paper'
     import QRCodeScanner from 'react-native-qrcode-scanner'

   
import Dimentions from '../modules/Dimensions'
import HeaderStyle from '../components/HeaderStyle';

class Profile extends Component {
    constructor(props){        
        super(props)        
        this.state={  
            title:'Profile'  ,
            UserName:'Leslie Alexander',
            Email:'lorem.ipsum@gmail.com',
            ProfileArr:[
                {ID:'1',Name:'My Account',image:require('../assets/icons/my_account.png')},
                {ID:'2',Name:'Preference',image:require('../assets/icons/preference.png')},
                {ID:'3',Name:'Information',image:require('../assets/icons/information.png')},
                {ID:'4',Name:'Access',image:require('../assets/icons/access.png')},

            ]      
        }   
    }

    renderItem = ({ item }) => (

          
                                    <View style={styles.renderItemContainer}>  
                                          <View style={styles.listRow} >
                                            <View style={styles.renderViewOne}>
                                                <View style={styles.renderViewTwo}>
                                                      <View style={styles.iconView}>
                                                            <Image
                                                                 source={item.image}
                                                                 style={styles.IconOne}
                                                             />
                                           
                                                        </View>     

                                                     <Text style={styles.textStyleTwo}>  {item.Name} </Text> 
                                              </View>
                                         </View> 
                                          <View style={styles.renderViewThree}>
                                              <Image
                                                  source={require('../assets/icons/front_arrow.png')}
                                                  style={styles.IconTwo}

                                               />
                                      </View>          
                                 </View>
                             </View>
        ) 

    render(){
        return(
            <View style={styles.container}>
                    <HeaderStyle title={this.state.title}/>
                    <View style={styles.profImageView}>
                                    <Image 
                                                 source={require('../assets/icons/user.png')}
                                                 style={styles.profImageStyle}
                                    />
                                    <Text style={styles.TextstyleFour}>{this.state.UserName}</Text>
                                    <Text style={styles.TextstyleFive}>{this.state.Email}</Text>

                    </View>
                    <View style={styles.flatListView}> 
                                     <FlatList
                                                  data={this.state.ProfileArr}
                                                  renderItem={this.renderItem}
                                                  keyExtractor={(item)=>item.ID}>
                                    </FlatList>
                    </View>
          </View>       


        )

    }
}
const styles = StyleSheet.create({
    container: {
      
        backgroundColor:'#FFFFFF',
        justifyContent:'center'
        
    },
    renderItemContainer:{
        flexDirection:'column',backgroundColor:'#FFFFFF'
    },
    listRow:{
     
        width:'100%',
        flexDirection:'row',
        borderBottomWidth:0.4,
        borderBottomColor:'black',
        paddingVertical:5
    },
    renderViewOne:{
        flexDirection:'column',marginBottom:10,marginLeft:5
    },
    renderViewTwo:{
        flexDirection:'row',marginLeft:10,
    },
    renderViewThree:{
        flexDirection:'row',flex:1,justifyContent:'flex-end'

    },
    flatListView:{
        backgroundColor:'#FFFFFF',
     

    },
    profImageView:{
        backgroundColor:'#E5E5E5',
        justifyContent:'center',
        alignSelf:'center',
        width:'100%',
    
    },
    profImageStyle:{
    
        height:180,
        width:180,
        borderRadius:180/2,
        marginLeft:15,
        alignSelf:'center',
        justifyContent:'center',
        margin:10,
        backgroundColor:'#E5E5E5',
    },
    textStyleTwo:{
    
        fontSize:14,
        color: '#171728',
        fontWeight: "800",
        fontFamily:'StolzlW00-Book',
        textAlign:'center',
        marginLeft:20,
        marginTop:15
      //  alignSelf:'center',
       // justifyContent:'center',
        //alignContent:'center',
       
     
    },
    TextstyleFour:{
           
        color: '#313144',
        fontSize:16,
        marginTop:15,
        fontFamily: 'StolzlW00-Medium',
        fontWeight:"bold",
        textAlign:'center',
        alignSelf:'center',
        justifyContent:'center',
       
              
    },
    IconOne:{
    
        height:25,
        width:25,
        alignSelf:'center',
        marginLeft:15

    },
    IconTwo:{
    
        height:12,
        width:10,
        margin:15,
        alignSelf:'center',
    },
    iconView:{
      
        height:40,
        width:40,
        backgroundColor:'#E5E5E5',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:40/2,
        flexWrap:'wrap',
        alignSelf:'center',
      

    },
    TextstyleFive:{
           
        color: '#646480',
        fontSize:14,
        marginTop:5,
        fontFamily: 'StolzlW00-Book',
        fontWeight:"normal",
        textAlign:'center',
        alignSelf:'center',
        justifyContent:'center',
        marginBottom:20
       
              
    },
})
export default Profile
   