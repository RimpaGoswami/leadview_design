import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,Linking,FlatList
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title,Card} from 'react-native-paper'
  
import HeaderStyle from '../components/HeaderStyle';

class Information extends Component {
    constructor(props){        
        super(props)        
        this.state={
           title:'Information',
           text:'Any query about your personal \ndata, please contact :'
        
    }
}
 

    render(){
        return(
            <View>
              
                    <HeaderStyle title={this.state.title}/>
                <ScrollView >    
                    <View style={styles.viewOne}>
                        <View style={{backgroundColor:'#FFFFFF'}}>
                                 <Text style={styles.textStyleOne}> IP Location  </Text>
                                 <Card style={styles.cardViewOne} >
                                    <View style={{flexDirection:'column',justifyContent:'center'}}> 
                                        <Text style={styles.textStyleTwo}>IP Region</Text>
                                        <Text style={styles.textStyleThree}>West Bengal</Text>
                                   </View>
                                   <View style={styles.viewTwo}>
                                                            <Image
                                                                  source={require('../assets/icons/address.png')}
                                                                  style={styles.iconOne}
                    
                                                            />

                                   </View>



                                </Card>
                                <Card style={styles.cardViewOne} >
                                    <View style={{flexDirection:'column'}}> 
                                        <Text style={styles.textStyleTwo}>IP Country</Text>
                                        <Text style={styles.textStyleThree}>India</Text>
                                   </View>
                                   <View style={styles.viewTwo}>
                                                            <Image
                                                                  source={require('../assets/icons/address.png')}
                                                                  style={styles.iconOne}
                    
                                                            />

                                   </View>



                                </Card>
                                <Card style={styles.cardViewOne} >
                                    <View style={{flexDirection:'column'}}> 
                                        <Text style={styles.textStyleTwo}>Browser language</Text>
                                        <Text style={styles.textStyleThree}>English</Text>
                                   </View>
                                   <View style={styles.viewTwo}>
                                                            <Image
                                                                  source={require('../assets/icons/address.png')}
                                                                  style={styles.iconOne}
                    
                                                            />

                                   </View>


                                </Card>
                                <Card style={styles.cardViewOne} >
                                    <View style={{flexDirection:'column'}}> 
                                        <Text style={styles.textStyleTwo}>First Visit</Text>
                                        <Text style={styles.textStyleThree}>03rd Nov 2020</Text>
                         
                                   </View>
                                   <View style={styles.viewTwo}>
                                                            <Image
                                                                  source={require('../assets/icons/address.png')}
                                                                  style={styles.iconOne}
                    
                                                            />

                                   </View>



                                </Card>
                                <Card style={styles.cardViewOne} >
                                    <View style={{flexDirection:'column'}}> 
                                        <Text style={styles.textStyleTwo}>Last Visit</Text>
                                        <Text style={styles.textStyleThree}>06 Nov 2020</Text>
                                   </View>
                                   <View style={styles.viewTwo}>
                                                            <Image
                                                                  source={require('../assets/icons/address.png')}
                                                                  style={styles.iconOne}
                    
                                                            />

                                   </View>



                                </Card>
                                <Card style={styles.cardViewLast} >
                                    <View style={{flexDirection:'column'}}> 
                                        <Text style={styles.textStyleTwo}>Total pages visited</Text>
                                        <Text style={styles.textStyleThree}>1</Text>
                                   </View>
                                   <View style={styles.viewTwo}>
                                                            <Image
                                                                  source={require('../assets/icons/address.png')}
                                                                  style={styles.iconOne}
                    
                                                            />

                                   </View>



                                </Card>

                        </View>
                      
                        <View style={styles.viewThree}>
                            <View style={styles.iconView}> 
                                            <Image
                                                                  source={require('../assets/icons/designation.png')}
                                                                  style={styles.iconTwo}
                    
                                             />

                          </View>  
                                            <Text style={styles.textStyleFour}>{this.state.text}</Text>
                            <View style={styles.viewTwo}>
                            <Image
                                                                  source={require('../assets/icons/information.png')}
                                                                  style={styles.iconThree}
                    
                                             />
                             
                             
                           </View>                   
                         </View>   
                        
                        
                   </View>    
                </ScrollView>        
            </View>
        )
    }
}
const styles = StyleSheet.create({
    textStyleOne:{
        fontSize:15,
        color: '#313144',
        fontWeight: "bold",
        fontFamily:'StolzlW00-Medium',
        marginTop:20,
        marginLeft:10
       // marginBottom:25
},
viewOne:{
    backgroundColor:'#E5E5E5',
    flex:1

},
viewTwo:{
    flexDirection:'row',
    flex:1,
    justifyContent:'flex-end'

},
viewThree:{
    marginTop:15,
    flexDirection:'row',
    backgroundColor:'#FFFFFF'

},
cardViewOne:{
    backgroundColor:'#FFFFFF',
    width:380,
    height:60,
    flexDirection:'row',
   // borderBottomRightRadius:0,
  //  borderBottomLeftRadius:0,
    borderRadius:10, 
    borderWidth:3,
    borderColor:'#FFFFFF',
    marginTop:20, 
    alignSelf:'center',
    justifyContent:"center"

},
textStyleTwo:{
    fontSize:14,
    color: '#171728',
    fontWeight: "normal",
    fontFamily:'StolzlW00-book',
    marginTop:5,
    marginLeft:15,
    justifyContent:'center',
    alignItems:"center",
    marginBottom:3
},
textStyleThree:{
    fontSize:14,
    color: '#646480',
    fontWeight: "normal",
    fontFamily:'StolzlW00-book',
    marginLeft:15,
    justifyContent:'center',
    alignItems:"center",
    marginBottom:3
},
cardViewLast:{
    backgroundColor:'#FFFFFF',
    width:380,
    height:50,
    flexDirection:'row',
   // borderBottomRightRadius:0,
  //  borderBottomLeftRadius:0,
    borderRadius:10, 
    borderWidth:3,
    borderColor:'#FFFFFF',
    marginTop:15, 
    alignSelf:'center',
    justifyContent:"center",
    marginBottom:20

},
iconOne:{
    height:30,
    width:25,
    justifyContent:'flex-end',
    alignSelf:'flex-end',
    marginBottom:15,
    marginTop:5,
    marginRight:20
},
iconThree:{
    height:30,
    width:30,
    justifyContent:'flex-end',
    alignSelf:'flex-end',
    marginBottom:25,
  
    marginRight:20
},
iconView:{
    height:60,
    width:65,
    backgroundColor:'#F2F4F5',
    justifyContent:'center',
    marginLeft:15,
    margin:10


},
iconTwo:{
    height:45,
    width:45,
    justifyContent:'flex-end',
    alignSelf:'flex-end',
    marginBottom:10,
    marginRight:10,
    marginTop:10,
   
},
textStyleFour:{
    fontSize:13,
    color: '#171728',
    fontWeight: "normal",
    fontFamily:'StolzlW00-book',
    marginLeft:5,
    justifyContent:'center',
    alignItems:"center",
    alignSelf:'center',
    marginBottom:3,

},

})
export default Information