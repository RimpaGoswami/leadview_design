import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,FlatList
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title,Card} from 'react-native-paper'


   
import Dimentions from '../modules/Dimensions'
   

     class HeaderStyle extends Component {
        constructor(props){        
            super(props);        
            this.state={    
                   
            } 

        }
        render()
        {
            return(
                <View style={styles.headerContainer}>

                        <Image
                                source={require('../assets/icons/back_arrow.png')}
                                style={styles.IconTwo}                     
                         />
                        <Text style={styles.headerText}>{this.props.title}</Text>
                </View>
            )
        }
    }
    const styles = StyleSheet.create({
        headerText:{
            fontWeight: "bold",
            color: '#313144',
            fontFamily: 'StolzlW00-Medium',
            fontSize:16,
           justifyContent:'center',
           marginLeft:15,
           alignSelf:'center'        
        },
    
        IconOne:{
        
            height:15,
            width:15,
            marginLeft:15,
            alignSelf:'center'
        },
    
        IconTwo:{
        
            height:20,
            width:25,
            marginLeft:10  ,
            marginTop:15
        },
        headerContainer:{
    
           marginTop:10,
            height:50,
            width:'100%',
            backgroundColor:'#FFFFFF',
           // borderBottomColor:'black',
            borderBottomWidth:0.2,
            flexDirection:'row',
          
        },
    })
    export default HeaderStyle