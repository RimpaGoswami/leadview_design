import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,FlatList
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title,Card} from 'react-native-paper'


   
import Dimentions from '../modules/Dimensions'
   

     class Header extends Component {
        constructor(props){        
            super(props);        
            this.state={    
            
            } 

        }
        render(){
            return(
                

                    <View style={styles.headerContainer}>
              
                   
                   <Text style={styles.headerText}>{this.props.title}</Text>
                   <View style={{flexDirection:'row',flex:1,justifyContent:'flex-end'}}>
                   <Image
                        source={require('../assets/icons/notification.png')}
                        style={styles.IconOne}
                
                
                
                     />
                    </View> 
                      <Image
                        source={require('../assets/icons/user.png')}
                        style={styles.IconTwo}
                
                
                
                     />
                   </View>
                

            )

        }

    }
    const styles = StyleSheet.create({
    headerText:{
        fontWeight: "bold",
        color: '#313144',
        fontFamily: 'StolzlW00-Medium',
        fontSize:16,
       justifyContent:'center',
       marginLeft:20,

     
      
      
    },

    IconOne:{
    
        height:25,
        width:25,
        alignSelf:'baseline',
        marginLeft:15,
        justifyContent:'flex-end',
         alignContent:'flex-end',
       // marginLeft:250   
    },

    IconTwo:{
    
        height:30,
        width:30,
        alignSelf:'baseline',
        marginLeft:15,
        justifyContent:'flex-end',
         alignContent:'flex-end',
        marginLeft:15  
    },
    headerContainer:{

       marginTop:20,
        height:40,
        width:'100%',
    
        backgroundColor:'#FFFFFF',
       // borderBottomColor:'black',
        borderBottomWidth:0.2,
        flexDirection:'row',
   

    },
})
    export default Header

