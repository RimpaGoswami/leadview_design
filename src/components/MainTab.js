import React,{Component} from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import Basket from '../screens/Basket';
import Message from '../screens/Message';
import Catalog from '../screens/Catalog';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { StyleSheet, 
  Text,
  View,
  TextInput,
  Button,
  StatusBar,
  TouchableOpacity,ActivityIndicator,Image,ScrollView
  
  } from 'react-native';


import More from '../screens/More';
const Tab = createBottomTabNavigator();

const MainTab=()=> {
  return (
    <Tab.Navigator
      initialRouteName="Catalog"
      tabBarOptions={{
        activeTintColor: '#e91e63',
      }}
    >
      <Tab.Screen
        name="Catalog"
        component={Catalog}
        options={{
            
          tabBarLabel: ()=>(<Text style={styles.TextStyle}>CATALOGUE</Text>),
          tabBarIcon: ({ color, size }) => (
            <Image source={require('../assets/icons/catalogue_2.png')} style={styles.IconOne}  />
          ),
        }}
      />
      <Tab.Screen
        name="Basket"
        component={Basket}
        options={{
          tabBarLabel: ()=>(<Text style={styles.TextStyle}>BASKET</Text>),
          tabBarIcon: ({ color, size }) => (
            <Image  source={require('../assets/icons/basket.png')} style={styles.IconOne} />
          ),
         
        }}
      />
      <Tab.Screen
        
        name="Message"
        component={Message}
        options={{
          tabBarLabel:  ()=>(<Text style={styles.TextStyle}>MESSAGE</Text>),
          tabBarIcon: ({ color, size }) => (
            <Image source={require('../assets/icons/message.png')} style={styles.IconOne} />
          ),
        
        }}
      />
       <Tab.Screen
        name="More"
        component={More}
        options={{
          tabBarLabel:  ()=>(<Text style={styles.TextStyle}>MORE</Text>),
          tabBarIcon: ({ color, size }) => (
            <Image source={require('../assets/icons/more.png')} style={styles.IconOne} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  IconOne:{
        
    height:25,
    width:25,
  
    
   
   
    
  },
  TextStyle:{
    color: '#313144',
    fontSize:12,
  
    fontFamily: 'StolzlW00-Book',
    fontWeight:"normal",
  

  }
})
export default MainTab

