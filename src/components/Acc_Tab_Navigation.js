import { NavigationContainer } from '@react-navigation/native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs'
import {createStackNavigator} from '@react-navigation/stack';
import MyAcc_Personal_Details from '../screens/MyAcc_Pesonal_Details'
import MyAcc_Other_Details from '../screens/MyAcc_Other_Details'
import MyAcc_Social_Details from '../screens/MyAcc_Social_Details'
import React,{Component} from 'react';
import My_Acc_Header from './My_Acc_Header'
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,Linking,FlatList
    
    } from 'react-native';

   /* const topTab=createMaterialTopTabNavigator({


        MyAcc_Personal_Details:{
                screen: MyAcc_Personal_Details,
         navigationOptions:{     
           
                 tabBarLabel: ({})=>
                    {
                    <Text style={styles.textStyleOne}>Personal Details</Text>
                    },
                 },
            },
        
        MyAcc_Other_Detail:{
            screen: MyAcc_Other_Details,
            navigationOptions:{
                tabBarLabel:({})=>{
                    <Text style={styles.textStyleOne}>Other Details</Text>  

                },
            },
        }  ,
        
        MyAcc_Social_Details:{
            screen: MyAcc_Social_Details,
            navigationOptions:{
                tabBarLabel:({})=>{
                    <Text style={styles.textStyleOne}>Other Details</Text>  

                },
            },
        } ,

    },
        {
            initialRouteName:'MyAcc_Personal_Details',
            lazy:'true',
           // tabBarPosition:'center',
            swipeEnabled:'true',
            tabBarOptions:{
                style:{
                    height:60,
                    backgroundColor:'#FFFFFF',
                    paddingBottom:3,

                },
                activeTintColor:'#23334C'
            }

        }

 )*/


 const Tab = createMaterialTopTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="MyAcc_Personal_Details"
      tabBarPosition='top'
      tabBarOptions={{
        activeTintColor: '#23334C',
        labelStyle: { fontSize: 12 },
      
        style: { backgroundColor: '#FFFFFF',marginTop:5},
      }}
    >
      <Tab.Screen
        name="MyAcc_Personal_Details"
    
        component={MyAcc_Personal_Details}
        options={{ tabBarLabel:()=>(<Text style={styles.textStyleOne}>Personal Details</Text>  )}}
      />
      <Tab.Screen
        name="MyAcc_Other_Details"
        component={MyAcc_Other_Details}
        options={{ tabBarLabel:()=>(<Text style={styles.textStyleOne}>Other Details</Text>  ) }}
      />
      <Tab.Screen
        name="MyAcc_Social_Details"
        component={MyAcc_Social_Details}
        options={{ tabBarLabel:()=>(<Text style={styles.textStyleOne}>Social Details</Text>  ) }}
      />
    </Tab.Navigator>
  );
}


    const Stack=createStackNavigator()
    const App =()=>{
    return(
        <NavigationContainer >
        <Stack.Navigator headerMode='none' >
       <Stack.Screen name="MyAcc_Personal_Details" component={MyTabs}
         
         />         
     </Stack.Navigator>
       
     
     </NavigationContainer>
    )
    }
         
export default App

    const styles = StyleSheet.create({
        textStyleOne:{
    
            fontSize:14,
            color: '#23334C',
            fontWeight: "bold",
            fontFamily:'StolzlW00-Medium',
            textAlign:'center',
       
          
            
        },
       
    })