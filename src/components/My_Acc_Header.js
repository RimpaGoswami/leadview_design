import React,{Component} from 'react';
import { StyleSheet, 
    Text,
    View,
    TextInput,
    Button,
    StatusBar,
    TouchableOpacity,ActivityIndicator,Image,ScrollView,Linking,FlatList
    
    } from 'react-native';
     import { Icon } from 'react-native-vector-icons'
     import {Avatar, Caption, Drawer, Title,Card} from 'react-native-paper'
     import QRCodeScanner from 'react-native-qrcode-scanner'

   
import Dimentions from '../modules/Dimensions'
import HeaderStyle from '../components/HeaderStyle';

class My_Acc_Header extends Component {
    constructor(props){        
        super(props)        
        this.state={  
            title:'My Account'  ,
            UserName:'Leslie Alexander',
            Email:'lorem.ipsum@gmail.com',
          
        }   
    }


    render(){
        return(
                <View style={styles.container}>
                     <HeaderStyle title={this.state.title}/>
                         <View style={styles.profImageView}>
                                 <Image 
                                         source={require('../assets/icons/user.png')}
                                         style={styles.profImageStyle}
                                 />
                                  <Text style={styles.TextstyleFour}>{this.state.UserName}</Text>
                                  <Text style={styles.TextstyleFive}>{this.state.Email}</Text>

                        </View>
                 </View>       
        )


    }
}
const styles = StyleSheet.create({
    container: {
      
        backgroundColor:'#FFFFFF',
        justifyContent:'center'
        
    },
    profImageView:{
        backgroundColor:'#E5E5E5',
        justifyContent:'center',
        alignSelf:'center',
        width:'100%',
    
    },
    profImageStyle:{
    
        height:180,
        width:180,
        borderRadius:180/2,
        marginLeft:15,
        alignSelf:'center',
        justifyContent:'center',
        margin:10,
        backgroundColor:'#E5E5E5',
    },
    TextstyleFour:{
           
        color: '#313144',
        fontSize:16,
        marginTop:15,
        fontFamily: 'StolzlW00-Medium',
        fontWeight:"bold",
        textAlign:'center',
        alignSelf:'center',
        justifyContent:'center',
       
              
    },
    TextstyleFive:{
           
        color: '#646480',
        fontSize:14,
        marginTop:5,
        fontFamily: 'StolzlW00-Book',
        fontWeight:"normal",
        textAlign:'center',
        alignSelf:'center',
        justifyContent:'center',
        marginBottom:20
       
              
    },
})
export default My_Acc_Header
